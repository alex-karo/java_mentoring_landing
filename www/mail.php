<?php

$name = $_POST["name"];
$phone = $_POST["phone"];
$location = $_POST["from"];

if(!$name || !$phone) {
    header("HTTP/1.1 400 OK");
    echo "Incorrect parameters";
}

$to = "sevostyanovg.d@gmail.com";
$from = "Java Landing <info@jaland.ru>";
$headers .= "From: webmaster@example.com\r\n";
$headers .= "Reply-To: webmaster@example.com\r\n";
$headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
$headers .= 'Content-type: text/plain; charset=utf-8' . "\r\n";

$subject = 'Feedback from landing';

$message = 'Заполнена форма "Обратная связь" на сайте каком-то'."\n";
$message .= 'Имя: '.$name."\n";
$message .= 'Телефон: '.$phone."\n";
$message .= 'Откуда: '.$location."\n";

if(!(mail($to, $subject, $message, $headers))){
    header("HTTP/1.1 500 OK");
    echo "Sorry, something bad happened...";
} else {
    echo "Success";
}