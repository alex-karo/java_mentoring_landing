$(function () {

    /**
     * FullPage and nav
     */
    $('.slides').fullpage({
        'anchors': ['-intro', '-about', '-advantages', '-reviews', '-pricing', '-start', '-faq'],
        'menu': '#menu',
        'verticalCentered': false,
        'scrollOverflow': true
    });

    $('.js-scroll-down').click(function (e) {
        e.preventDefault();
        $.fn.fullpage.moveSectionDown();
    });

    $('.hamburger').click(function (e) {
        e.preventDefault();

        var $menu = $('.main_menu');
        var $burger = $(this);

        $menu.toggleClass('is-active');
        $burger.toggleClass('is-active');

        if($menu.hasClass('is-active')) {
            $('.slides').one('click', function () {
                $menu.removeClass('is-active');
                $burger.removeClass('is-active');
            })
        }
    });

    $('#menu').click(function () {
        $('.main_menu').removeClass('is-active');
        $('.hamburger').removeClass('is-active');
    });

    /**
     * Slider
     */
    var reviewPhotos = [];

    $('.review__photo').each(function () {
        reviewPhotos.push($(this).attr('src'));
    });

    var $slick = $('.reviews__list').slick({
        arrows: true,
        dots: true,
        dotsClass: 'reviews__dots'
   });

    $('.reviews__dots button').each(function () {
        $(this).css('background-image', 'url("' + reviewPhotos.shift() + '")');
    });

    /**
     * Pricing and timer
     */

    $('.js-change-hover').on('click tap mouseenter', function () {
       $('.js-change-hover.active').removeClass('active');
        $(this).addClass('active');
    });

    var dateForTimer = $('.js-timer').data('time-end');

    $('.js-timer').timeTo({
        'displayDays': 2,
        'timeTo': new Date(dateForTimer),
        'displayCaptions': true,
        'lang': 'ru',
        'fontFamily': 'Exo 2'
    });

    /**
     * Accordion
     */

    var $objects = $('.js-accordion-item');
    $objects.on('click tap', '.faq__question', function (e) {
        e.preventDefault;

        var $this = $(e.delegateTarget);
        $objects
            .not($this)
            .filter('.is-active')
            .removeClass('is-active');

        $this.toggleClass('is-active');
        $.fn.fullpage.reBuild();
    });

    /**
     * Modal
     */

    $(".js-phone-mask").inputmask({"mask": "+7 (999) 999-99-99"});;

    function openModal(elem) {
        var modal = $(elem);
        modal.fadeIn(400, function () {
            modal.one('click tap', '.modal__parangua, .modal__close', function (e) {
                e.preventDefault();
                closeModal(modal);
            });
        });
    }

    function closeModal(elem) {
        var modal = $(elem);
        modal.fadeOut(400);
        modal.off('click tap');
    }

    $('.js-open-modal').click(function (e) {
        e.preventDefault();
        var from = $(this).data('from');
        $('#modal_from').val(from);
        openModal('#feedback_modal');
    });

    /**
     * AJAX Send
     */

    var ajax_modal = $('#ajax_modal');
    var ajax_modal_title = $('#modal_title');
    var ajax_modal_text = $('#modal_text');

    $('.feedback_form').on('submit', function (e) {
        e.preventDefault();
        closeModal('#feedback_modal');

        var formData = $(this).serialize();
        var url = $(this).attr('action');
        var data = {
            url: url,
            type: 'POST',
            processData: false,
            data: formData
        };
        $(this).find('input').val('');

        $.ajax(data).done(function (data) {
            if (data === "Incorrect parameters") {
                ajax_modal_title.text('Ошибка');
                ajax_modal_text.text('Пожалуйста заполните поля');
                openModal(ajax_modal);
                console.error(data);
                return;
            }

            if (data !== "Success") {
                ajax_modal_title.text('Извините');
                ajax_modal_text.text('Что-то пошло не так.');
                openModal(ajax_modal);
                console.error(data);
                return;
            }

            ajax_modal_title.text('Спасибо!');
            ajax_modal_text.text('Ваша заявка отправлена');
            openModal(ajax_modal);
        }).fail(function (data) {
            ajax_modal_title.text('Извините');
            ajax_modal_text.text('Что-то пошло не так.');
            openModal(ajax_modal);
            console.error(data);
        });
    });
});